FROM python:alpine
ADD ./sample /code
ADD requirements.txt /code
WORKDIR /code
RUN ["apk",  "add", "--update", "curl", "gcc",  "g++", "libpng", "freetype-dev"]
RUN ["ln", "-s", "/usr/include/locale.h", "/usr/include/xlocale.h"]
RUN pip install -r requirements.txt
CMD ["export", "FLASK_APP=app.py"]
CMD ["python", "app.py"]
