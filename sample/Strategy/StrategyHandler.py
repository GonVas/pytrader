class StrategyHandler:

    def __init__(self, strat, params):
        self.params = params
        self.strat = strat.run_strategy(params)

    def get_final_equity(self):
        return self.strat.get_final_equity()

    def get_final_info(self):
        return self.strat.get_info()
       