import os
import datetime

from pyalgotrade import strategy
from pyalgotrade.barfeed import quandlfeed
from pyalgotrade.technical import ma

from .Strategy import Strategy


class smaexample(Strategy, strategy.BacktestingStrategy):


    def __init__(self, feed, instrument, smaPeriod):
        super(smaexample, self).__init__(feed, 1000)
        self.__position = None
        self.__instrument = instrument
        # We'll use adjusted close values instead of regular close values.
        self.setUseAdjustedValues(True)
        #import pdb; pdb.set_trace();
        self.__sma = ma.SMA(feed[instrument].getPriceDataSeries(), smaPeriod)


    def correct_params(self, params):
        pass

    def run_strat(self):
        pass

    def onEnterOk(self, position):
        execInfo = position.getEntryOrder().getExecutionInfo()
        self.info("BUY at $%.2f" % (execInfo.getPrice()))

    def onEnterCanceled(self, position):
        self.__position = None

    def onExitOk(self, position):
        execInfo = position.getExitOrder().getExecutionInfo()
        self.info("SELL at $%.2f" % (execInfo.getPrice()))
        self.__position = None

    def onExitCanceled(self, position):
        # If the exit was canceled, re-submit it.
        self.__position.exitMarket()

    def onBars(self, bars):
        # Wait for enough bars to be available to calculate a SMA.
        if self.__sma[-1] is None:
            return

        bar = bars[self.__instrument]
        # If a position was not opened, check if we should enter a long position.
        if self.__position is None:
            if bar.getPrice() > self.__sma[-1]:
                # Enter a buy market order for 10 shares. The order is good till canceled.
                self.__position = self.enterLong(self.__instrument, 10, True)
        # Check if we have to exit the position.
        elif bar.getPrice() < self.__sma[-1] and not self.__position.exitActive():
            self.__position.exitMarket()


    def get_info(self):
        info = {'equity':self.getBroker().getEquity(), 'position':self.getBroker().getPositions()}
        return info

    @classmethod
    def get_requeriments(cls):
        reqs = {'numb_insts':1, 'start_money':True, 'output':1, 'dates':True}
        return reqs

    @classmethod
    def run_strategy(cls, params, smaPeriod=15):
        # Load the bar feed from the CSV file

        datadw = cls.get_datadownloader()
        dates = [int(params['fromdate'].split('-')[0]), int(params['todate'].split('-')[0])]

        # Evaluate the strategy with the feed.
        myStrategy = cls(datadw.download_feed_single(params['target'], dates), params['target'], smaPeriod)
        myStrategy.run()
        return myStrategy