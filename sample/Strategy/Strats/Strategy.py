import os
import datetime
from .. import Data

class Strategy():

    @classmethod
    def get_requeriments(cls):
        # must return dictionary with requeriments
        # mandatory : numb_insts -> INT, start_money -> BOOL, output -> INT/ENUM,
        #             dates -> BOOL 
        # output on dict:
        # 1 -> Single or same insts as numb_insts
        # 2 -> Algorthim picks itself
        raise NotImplementedError('Strategies must specify requeriments')

    @classmethod
    def run_strategy(cls, params):
        # Must run algorithm and return instance of the class Strategy 
        raise NotImplementedError('Strategies must override run_strategy(cls, params) and return Strategy!')

    @classmethod
    def get_datadownloader(cls):
        return Data.DataDownloader.DataDownloader()

    @classmethod
    def build_feed_textbox(cls, textbox, dates):
        pass

    def get_info(self):
        # return dictonary with info
        # mandatory : equity -> FLOAT, position : dict{inst(STR) : quantity(INT)}
        raise NotImplementedError('Strategies must return basic info')