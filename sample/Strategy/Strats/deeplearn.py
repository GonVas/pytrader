import os
import datetime

from pyalgotrade import strategy
from pyalgotrade.barfeed import quandlfeed

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.models import load_model

from sklearn.preprocessing import MinMaxScaler
from pandas import Series

import matplotlib.pyplot as plt

import numpy

from .Strategy import Strategy


numpy.random.seed(7)

class deeplearn(Strategy, strategy.BacktestingStrategy):


    def __init__(self, inputs_insts, target_feed, instrument, initmoney, dates):
        super(deeplearn, self).__init__(target_feed, initmoney)
        self.__position = None
        self.__instrument = instrument

        self.target_inst = instrument

        # We'll use adjusted close values instead of regular close values.
        self.setUseAdjustedValues(True)

        self.set_configs()
        self.create_train_net(inputs_insts, instrument, dates)

    def set_configs(self):

        self.days_pred= 30 # days to predict

        # LSTM Parameters
        self.window_mul = 20 # Window for lstm net
        self.epochs = 70
        self.batch_size_mult = 1 # this mult by window is batch size

        #Params checking
        if(not (self.window_mul % int(self.batch_size_mult*self.window_mul) == 0 or
           self.window_mul % int(self.batch_size_mult*self.window_mul) == self.window_mul)):
            raise ValueError('Inconsistent Parameters -> batch_size_mult')

    def scale_data(self, data):
        series = Series(data)
        # prepare data for normalization
        values = series.values
        values = values.reshape((len(values), 1))
        # train the normalization
        scaler = MinMaxScaler(feature_range=(0, 1))
        scaler = scaler.fit(values)

        # normalize the dataset and print
        normalized = scaler.transform(values)

        self.scaler = scaler
        return normalized

    def create_data_lstm(self, training_set, numb_inst):
        # Creating a data structure with window timesteps and 1 output
        self.window = self.window_mul*numb_inst
        self.numb_inst = numb_inst

        X_train = []
        y_train = []

        grouped_set = numpy.reshape(training_set, (training_set.shape[0]//numb_inst, numb_inst))

        for i in range(self.window, len(grouped_set)):
            X_train.append(grouped_set[i - self.window: i])
            #print(grouped_set[i - self.window: i])
            y_train.append(grouped_set[i][0])

        X_train, y_train = numpy.array(X_train), numpy.array([y_train])
        X_train = numpy.reshape(X_train, (X_train.shape[0], X_train.shape[1], numb_inst))
        y_train = numpy.reshape(y_train, (y_train.shape[1], y_train.shape[0]))
        return (X_train, y_train)

    def create_lstm_model(self):
        regressor = Sequential()
        regressor.add(LSTM(units = 50, return_sequences = True, input_shape = (self.window, self.numb_inst)))
        regressor.add(Dropout(0.2))
        regressor.add(LSTM(units = 50, return_sequences = True))
        regressor.add(Dropout(0.2))
        regressor.add(LSTM(units = 50, return_sequences = True))
        regressor.add(Dropout(0.2))
        regressor.add(LSTM(units = 50))
        regressor.add(Dropout(0.2))
        regressor.add(Dense(units = 1))
        regressor.compile(optimizer = 'adam', loss = 'mean_squared_error', metrics=['accuracy'])

        return regressor

    def get_data(self, inputs_insts, target_inst, dates):

        self.datadw = self.get_datadownloader()
        inputs_data = []
        for input_inst in inputs_insts:
            inputs_data.append(self.datadw.get_attribute_csv(input_inst,
                                                        dates, 'Adj. Close'))

        if(target_inst not in inputs_insts):
            inputs_data.append(self.datadw.get_attribute_csv(target_inst,
                                                        dates, 'Adj. Close'))

        inputs_data = numpy.array(self.weaveflat_data(inputs_data))
        inputs_data = self.scale_data(inputs_data)

        return self.create_data_lstm(inputs_data, len(inputs_insts))

    def weaveflat_data(self, inputs_data):
        final_data = []
        for i in range(len(inputs_data[0])):
            for j in range(len(inputs_data)):
                final_data += [inputs_data[j][i]]

        if(len(final_data) != len(inputs_data)*len(inputs_data[0])):
            print('weaveflat_data: Inconsistent data return null')
            return None

        return final_data


    def plot_predictions(self, predictions, real_results):     
        pred_string = '$' + self.target_inst + ' Stock Price Prediction'
        real_price_string  = 'Actual $' + self.target_inst + ' Stock Price'
        
        predictions_inverse = self.scaler.inverse_transform(predictions)
        real_results_inverse = self.scaler.inverse_transform(real_results)

        plt.figure(figsize=(10,6)) 
        plt.plot(real_results_inverse, color='blue', label=real_price_string)  
        plt.plot(predictions_inverse , color='red', label=pred_string)  
        plt.title(pred_string)  
        plt.xlabel('Date (days after testing)')  
        plt.ylabel(real_price_string)  
        plt.legend()  
        plt.show()  


    def evaluate_model(self, inputs_data, output_data, inputs_test_data, output_test_data, dates):
        scores = self.model.evaluate(inputs_data, output_data)

        predictions = self.model.predict(inputs_test_data)

        print("\n%s: %.2f%%" % (self.model.metrics_names[1], scores[1]*100))

        self.plot_predictions(predictions, output_test_data)

    def create_train_net(self, inputs_insts, target_inst, dates):
        inputs_data, output_data = self.get_data(inputs_insts, target_inst, dates)
        
        inputs_test_data = inputs_data[:self.days_pred]
        output_test_data = output_data[:self.days_pred]
        inputs_data, output_data = inputs_data[self.days_pred:], output_data[self.days_pred:]

        self.model = self.create_lstm_model()

        self.model.fit(inputs_data, output_data, epochs = self.epochs, shuffle=False, batch_size = int(self.window*self.batch_size_mult))

        self.evaluate_model(inputs_data, output_data, inputs_test_data, output_test_data,  dates)

        self.model.save('lstm_model.h5')

        return True

    def onEnterOk(self, position):
        execInfo = position.getEntryOrder().getExecutionInfo()
        self.info("BUY at $%.2f" % (execInfo.getPrice()))

    def onEnterCanceled(self, position):
        self.__position = None

    def onExitOk(self, position):
        execInfo = position.getExitOrder().getExecutionInfo()
        self.info("SELL at $%.2f" % (execInfo.getPrice()))
        self.__position = None

    def onExitCanceled(self, position):
        # If the exit was canceled, re-submit it.
        self.__position.exitMarket()

    def onBars(self, bars):
        # Wait for enough bars to be available to calculate a SMA.

        bar = bars[self.__instrument]
        # If a position was not opened, check if we should enter a long position.

    def get_info(self):
        info = {'equity':self.getBroker().getEquity(), 'position':self.getBroker().getPositions()}
        return info

    @classmethod
    def get_requeriments(cls):
        reqs = {'numb_insts':1, 'start_money':True, 'output':1, 'dates':True}
        return reqs

    @classmethod
    def run_strategy(cls, params):
        datefrom, dateto = int(params['fromdate'].split('-')[0]), int(params['todate'].split('-')[0])
        
        data_dw = cls.get_datadownloader()
        target_feed = data_dw.download_feed_single(params['target'], [datefrom, dateto])
        data_dw.download_instruments(params['insts'], datefrom, dateto)

        deeplearn = cls(params['insts'], target_feed, params['target'], float(params['startmoney']), [datefrom, dateto])
        deeplearn.run()
        return deeplearn



"""

example of csvs in the data_downloader:
self.get_datadownloader().get_attribute_csv('orcl', [2014, 2016], 'Adj. Close')
csv from data downloader
['37.844028143402',
 '38.080245744297',
 '38.07040334426',
 '38.208196944782',
 '38.168827344633',
 .................] array -> shape (total_days,  ) ex : (756,)

                                                returns -> inputs_data
===============================================================================
        in self.get_data()

 join all the different instruments prices in one array
ex:(amd, orcl) dates -> (2014, 2016)

lits -> [['35.404017924635', '35.781996763688', '35.898297944935'], ...,
       ['2.59', '2.49', '2.53']]

       shape -> (numb_ints, numb_years*total_days ), this example -> (2, 756)
                                
        list([orcl_price1, orclprice2, ...],[..., amd_price1, amdprice2, ...]])
                                                    
                                                    returns -> inputs_data    
===============================================================================
        in self.weaveflat_data(data)

uses the previous lists of lists of instruments and interweaves each instrument
and also flattens array. Returns None with there are instruments with different
days.

list([orcl_price1, orclprice2, ...],[..., amd_price1, amdprice2, ...]])
                    shape -> (numb_ints, numb_years*total_days )

becomes:

list([orcl_price1, amd_price1, orcl_price2, amdprice2, ...])
                    shape -> (numb_ints*numb_years*total_days )
                            ex: (1512,)

                                                    returns -> inputs_data
===============================================================================
        in self.scale_data(data)

uses the previous flatten prices arrays and scales it and reshapes it.

array([orclprice1, amdprice1, ..., orclprice2, amdprice2, ...]) shape -> (1512,)

array(['35.404017924635', '35.781996763688', '35.898297944935', ...,
       '2.59', '2.49', '2.53'], dtype='<U15')

becomes:

array([ [norm_orclprice1],
        [norm_amdprice1],
          ...,
        [norm_orclprice1],
        [norm_amdprice2],
         .............]]) shape -> (numb_ints*numb_years*total_days , 1)

ex:
array([[0.79369239],
       [0.02278834],
       [0.80530457],
       ...,
       [0.02278834],
       [0.07543902], 
       [0.02137875]]) shape -> (1512, 1)
                                                    returns -> inputs_data         
===============================================================================
        in create_data_lstm(self, training_set, num_insts)

uses inputs_data from previous functions and creates the final lstm input with
a window of size window_mul*numb insts  . The shape will be 
    

array([ [norm_orclprice1],
        [norm_amdprice1],
          ...,
        [norm_orclprice1],
        [norm_amdprice2],
         .............]]) shape -> (numb_ints*numb_years*total_days , 1)

becomes:

array([[[norm_orc1],        array([[[0.79369239],
        [norm_amd1],                [0.02936641],
        [norm_orc2],                [0.80257229],
        [norm_amd2]],               ...,
        
       [[norm_orcl3],               [0.85357483],
        [norm_amd3],                [0.01550547],
        [norm_orc4],                [0.82283669]],          
        ....,
                                   [[0.02936641],
                                    [0.80257229],
                                    [0.03195066],
                                    ...,
                                    [0.01550547],
                                    [0.82283669],
                                    [0.01433081]]]

window_size = self.window_mul*numb_insts
shape -> (numb_ints*numb_years*total_days-window_size, window_size ,1)
this example : shape -> (1474, 40, 1)

output_data
    will become the first instrument price + window_size, correpondes

array([norm_orcl_wd, norm_orcl_wd1, norm_orcl_wd2, norm_orcl_wd3,
       norm_orcl_wd4, .............................]) orcl -> target inst

    to shape -> ((numb_ints*numb_years*total_days-window_size)/numb_ints, )
     or shape -> (initial_input_data/numb_ints, )

array([0.8431011 , 0.85516866, 0.84924872, 0.85403021, 0.85585173,
       0.86040553, 0.85311945, 0.85767325, 0.85038717, 0.8431011 ,
       0.83012277, 0.82989508, 0.81122451, 0.84150727, 0.85084255,
       0.87087927, 0.87497768, 0.88681756, 0.88066993, 0.8836299 ,
       0.87725458, 0.86700854, 0.84628876, 0.84674414, 0.84719952,
       0.83490426, 0.83558733, 0.82875663, 0.82238131, 0.79574159,
       .............................])

        ex : shape -> (737, )

                                        returns -> inputs_data, output_data        
===============================================================================

"""