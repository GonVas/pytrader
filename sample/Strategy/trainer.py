import numpy
import time
import matplotlib.pyplot as plt
from pandas import read_csv
import math
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error

# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back-1):
		a = dataset[i:(i+look_back), 0]
		dataX.append(a)
		dataY.append(dataset[i + look_back, 0])
	return numpy.array(dataX), numpy.array(dataY)

def test_train_data(csv, ratio=0.67):
	#train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]
	cuttoff = int(ratio * len(csv))
	#train = instrument_csv[ ['Date', 'Close'] ][0:cuttoff]
	#test = instrument_csv[ ['Date', 'Close'] ][cuttoff:]
	train = csv[ 'Close'][0:cuttoff].values.astype('float32')
	test = csv['Close'][cuttoff:].values.astype('float32')
	
	# normalize the dataset
	scaler = MinMaxScaler(feature_range=(0, 1))

	return (test.reshape(-1, 1), train.reshape(-1, 1))
	#return (scaler.fit_transform(test), scaler.fit_transform(train))


def train_on_instrument(instrument_csv, test_ratio=0.67):
	dataframe = read_csv(instrument_csv, usecols=[1], engine='python', skipfooter=3)
	#dataset = dataframe.values
	#dataset = dataset.astype('float32')

	# split into train and test sets
	train, test = test_train_data(dataframe)

	# reshape into X=t and Y=t+1
	look_back = 1
	trainX, trainY = create_dataset(train, look_back)
	testX, testY = create_dataset(test, look_back)
	# reshape input to be [samples, time steps, features]
	trainX = numpy.reshape(trainX, (trainX.shape[0], trainX.shape[1], 1))

