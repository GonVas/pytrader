from Strategy.Data import DataDownloader
from .StrategyHandler import StrategyHandler
import datetime
import glob
import os
from mongoengine import connect

from Strategy.Strats import smaexample, deeplearn


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class StrategyManager(metaclass=Singleton):

    def __init__(self):
        connect()
        self._strategies = []
        #self._find_parse_strat()
        self.lastcall = datetime.datetime.now()
        self._strats = {'smaexample.py': smaexample.smaexample, 
                        'deeplearn.py': deeplearn.deeplearn}

    def _add_strategy(self, strategy):
        for strat in self._strategies:
            if strat.name == strategy.name:
                return False #change to exeption

        self.strategies.append(strategy)
        return True

    def run_strategy(self, name, params):
        params['insts'] = params['insts'].splitlines()
        print(params['insts'])
        return StrategyHandler(self._strats[name], params)

    def get_allstrats(self, strat_loc='/home/gonvas/Programming/pytrader/sample/Strategy/Strats/'):
        strat_names = []
        for strat in self._strats.items():
            strat_names.append(strat[0])
        return strat_names

    def get_lastcall(self):
        return str(self.lastcall)