from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import *
import datetime


class DayInfo(EmbeddedDocument):
    #Date,Open,High,Low,Close,Volume,Ex-Dividend,Split Ratio,Adj. Open,Adj. High,Adj. Low,Adj. Close,Adj. Volume
    Date =  DateTimeField(required=True)
    Open = FloatField(required=True)
    High = FloatField()
    Low = FloatField()
    Close = FloatField(required=True)
    Volume = FloatField()
    ExDividend = FloatField()
    SplitRatio = FloatField()
    OpenAdj = FloatField()
    HighAdj = FloatField()
    LowAdj = FloatField()
    CloseAdj = FloatField()
    VolumeAdj = FloatField()


class Instrument(Document):
    name = StringField(required=True)
    inst_type = StringField(required=True)
    created = DateTimeField(default=datetime.datetime.utcnow)
    start_date = DateTimeField()
    end_date = DateTimeField()
    trade_info = ListField(EmbeddedDocument(DayInfo))
