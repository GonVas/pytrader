import csv
import os

from pyalgotrade import strategy
from pyalgotrade.tools import quandl
from pyalgotrade.barfeed import quandlfeed
from threading import Thread

class DataDownloader:

    @staticmethod
    def build_feed(feeds, inst, index, from_year, to_year, folder="./inst_data"):
        try:
            feeds.append(quandl.build_feed("WIKI", [inst], from_year, to_year, folder))
        except :
            print("ERROR on getting data for instrument: ${}, date: {}-{}".format(inst, from_year, to_year))
        finally:
            pass

    @staticmethod
    def build_feeds(feeds, instruments, from_year=2010, to_year=2015, debug=0):
    
        if(to_year > 2017):
            print('Cannot get instrument data after 2017, defaulting to 2017.')
            to_year = 2017

        threads = []
        # In this case 'urls' is a list of urls to be crawled.
        for ii in range(len(instruments)):
            # We start one thread per url present.
            process = Thread(target=DataDownloader.build_feed, args=[feeds, instruments[ii], ii, from_year, to_year])
            process.start()
            threads.append(process)

        # We now pause execution on the main thread by 'joining' all of our started threads.
        # This ensures that each has finished processing the urls.
        for process in threads:
            process.join()

        if debug == 1:
            for feed in feeds:
                print("Last Registed feed {}".format( feed.getDefaultInstrument()))

    @staticmethod
    def download_instruments_file(file='instruments.txt'):
        instruments = []
        with open(file, "r") as inst_file:
            for line in inst_file.read().splitlines():
                if('#' not in line and len(line) > 0):
                    print("Added {}".format(line))
                    instruments.append(line)

        feeds = []
        DataDownloader.build_feeds(feeds, instruments)
        return feeds

    @staticmethod
    def download_instruments(insts, fromdate, todate):
        feeds = []
        DataDownloader.build_feeds(feeds, insts, fromdate, todate)
        return feeds

    @staticmethod
    def download_feed_single(inst, dates, folder="./inst_data"):
        feed = quandlfeed.Feed()
        return quandl.build_feed("WIKI", [inst], dates[0], dates[1], folder)

    @staticmethod
    def get_attribute_csv(inst, dates, attribute, folder="./inst_data", desc=True):
        flat_list_att = []
        for year in range(dates[0], dates[1]+1)[::-1]:
            with open(folder+'/WIKI-'+str(inst)+'-'+str(year)+'-quandl.csv', mode='r') as csv_file:
                csv_reader = csv.DictReader(csv_file)
                for day in csv_reader:
                    flat_list_att += [day[attribute]]
        if not desc:
            flat_list_att = flat_list_att[::-1]
        return flat_list_att



if __name__ == "__main__":
    DataDownloader.download_instruments()