from flask import Flask, render_template, request, g, jsonify
from Strategy.StrategyManager import StrategyManager
from Strategy.StrategyHandler import StrategyHandler

app = Flask(__name__)


def startup():
	print('Running startup Script')
	app.config['StrategyMan'] = StrategyManager()


startup()


inputs = {'dates':'''   <br> From date to train
                        <input type="date" name="fromdate">

                        <br> To date to train
                        <input type="date" name="todate">  ''',
         'target_single':'''                         
         				<br> Target instrument or portfolio:<br>
  						<input type="text" name="target"><br>''',
  		 'inputs_insts':'''
  		                <br> <p> Enter instruments inputs one per line.</p>
                        <textarea name="instruments" form="stratform"></textarea> '''
         }


@app.route('/')
def hello_world():
    #return ('Hello, World!, ' + app.config['StrategyMan'].get_lastcall())
    print(app.config['StrategyMan'].get_allstrats())
    return render_template('index.html', strats=app.config['StrategyMan'].get_allstrats())

def results_page(strat_handl):
	return jsonify(strat_handl.get_final_info())

@app.route('/runstrat', methods=['POST'])
def run_strat():
	strat = request.form['strat']
	insts = request.form['instruments']
	target = request.form['target']
	fromdate = request.form['fromdate']
	todate = request.form['todate']
	startmoney = request.form['startingmoney']
	input_args = {'insts':insts, 'target':target, 'fromdate':fromdate, 'todate':todate, 'startmoney':startmoney}
	return results_page(app.config['StrategyMan'].run_strategy(strat, input_args))
