# pytrader

Pytrader is a deep learning model that tries to predict the price usign a deep LSTM network.
Current resutls are can be seen in the `orcprice.png` and a pre trained network (trained for a couple of hours in a gtx 970) is avaiable in `lstm_model.h5`.


Uses [pyalgotrade](http://gbeced.github.io/pyalgotrade/docs/v0.20/html/index.html)

Example of prediction of price on ORCL stock:

![alt text](orclprice.png "Pricing Prediction of Oracle")